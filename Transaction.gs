Transaction = function(row, unit) {
  this.unit = unit;
  this.rate = MathUtil.round(row.rate, 2); 
  this.nav = MathUtil.round(row.nav, 2);
  this.commission = MathUtil.round(row.commission, 2);
  this.dateOfTransaction =  DateUtil.getDateString(row.dateOfTransaction);
  this.quantity = MathUtil.round(row.quantity, CONFIG_OBJECT.quantityRoundOff);
  this.transactionType = row.transactionType;
}

Transaction.prototype.splitTransaction = function(quantity) {
  quantity = Math.abs(quantity);
  var oldAbsoluteQuantity = this.getAbsoluteQuantity();
  ValidationUtil.isGreater(oldAbsoluteQuantity, quantity, "Spliting quantity is greater than the Transaction's quantity");
  var oldCommission = this.commission;
  this.setQuantity(MathUtil.round(this.getSign() * (oldAbsoluteQuantity - quantity), CONFIG_OBJECT.quantityRoundOff));
  this.commission  = MathUtil.round(oldCommission * this.getAbsoluteQuantity()/oldAbsoluteQuantity, 2);
  var newTransaction = ObjectUtil.clone(this);
  newTransaction.setQuantity(MathUtil.round(newTransaction.getSign() * quantity, CONFIG_OBJECT.quantityRoundOff));
  newTransaction.commission = MathUtil.round(oldCommission * newTransaction.getAbsoluteQuantity()/oldAbsoluteQuantity, 2);
  return newTransaction;
}

Transaction.prototype.getBlackListedPropertyArray = function() {
  var blackList = ["unit"];
  return blackList;
}

Transaction.prototype.stringify = function() {
  return JSON.stringify(this, ObjectUtil.getWhiteListedPropertyArray(this, this.getBlackListedPropertyArray()));
}

Transaction.prototype.writeToSheet = function(sheet, sheetHeader) {
  var transactionStr = this.stringify();
  var unitStr = this.unit.stringify();
  var row = ObjectUtil.getMergedObject(JSON.parse(unitStr), JSON.parse(transactionStr));
  row.quantity = Math.abs(row.quantity);
  LogUtil.log(arguments.callee.name, "d", "Row: " + JSON.stringify(row));
  SpreadsheetUtil.writeARow(sheet, sheetHeader, row);
}

Transaction.prototype.getUnit = function() {
  return this.unit;
}

Transaction.prototype.setRate = function(rate) {
  this.rate = this.rate && rate;
  this.nav = this.nav && rate;
}

Transaction.prototype.getRate = function() {
  // checking isNaN because we are converting them to number using MathUtil.round(number, n)
  if(isNaN(this.rate))
    return this.nav;
  else
    return this.rate;
}

Transaction.prototype.getSign = function() {
  var sign = this.isIncremental() ? 1 : -1;
  return sign;
}

Transaction.prototype.getQuantity = function() {
  return this.quantity;
}

Transaction.prototype.setQuantity = function(quantity) {
  this.quantity = quantity;
}

Transaction.prototype.getAbsoluteQuantity = function() {
  return Math.abs(this.quantity);
}

Transaction.prototype.isIncremental = function() {
  return this.incremental;
}

Transaction.prototype.getTransactionType = function() {
  return this.transactionType;
}

Transaction.prototype.getDateOfTransaction = function() {
  return this.dateOfTransaction;
}

Transaction.prototype.getCommission = function() {
  return this.commission;
}