var testCases = {
// Buy Transaction
  testBuyTransaction : function () {
    var r1 = {"unitCode":"S1Code","unitName":"S1","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"Buy","quantity":300,"rate":25,"commission":1};
    var t1 = TransactionFactory.createTransaction(r1);
    unittest.assertEquals(true, t1.isIncremental());
    unittest.assertEquals(1, t1.getSign());
    unittest.assertEquals(BuyTransaction, t1.constructor);
    compareTransactions(t1, r1);
    var n1t1 = t1.splitTransaction(10);
    unittest.assertEquals(290, t1.getQuantity());
    unittest.assertEquals(MathUtil.round(1*290/300, 2), t1.getCommission());
    var n1r1 = {"unitCode":"S1Code","unitName":"S1","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"Buy","quantity":10,"rate":25,"commission":MathUtil.round(1*10/300, 2)};
    unittest.assertEquals(true, n1t1.isIncremental());
    unittest.assertEquals(1, n1t1.getSign());
    unittest.assertEquals(BuyTransaction, n1t1.constructor);
    compareTransactions(n1t1, n1r1);
    var n2t1 = t1.splitTransaction(-20);
    unittest.assertEquals(270, t1.getQuantity());
    unittest.assertEquals(MathUtil.round(1*270/300, 2), t1.getCommission());
    var n2r1 = {"unitCode":"S1Code","unitName":"S1","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"Buy","quantity":20,"rate":25,"commission":MathUtil.round(1*20/300, 2)};
    unittest.assertEquals(true, n2t1.isIncremental());
    unittest.assertEquals(1, n2t1.getSign());
    unittest.assertEquals(BuyTransaction, n2t1.constructor);
    compareTransactions(n2t1, n2r1);
    var n3t1 = t1.splitTransaction(0);
    unittest.assertEquals(270, t1.getQuantity());
    unittest.assertEquals(MathUtil.round(1*270/300, 2), t1.getCommission());
    var n3r1 = {"unitCode":"S1Code","unitName":"S1","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"Buy","quantity":0,"rate":25,"commission":0};
    unittest.assertEquals(true, n3t1.isIncremental());
    unittest.assertEquals(1, n3t1.getSign());
    unittest.assertEquals(BuyTransaction, n3t1.constructor);
    compareTransactions(n3t1, n3r1);  
    var n4t1 = t1.splitTransaction(270);
    unittest.assertEquals(0, t1.getQuantity());
    unittest.assertEquals(0, t1.getCommission());
    var n4r1 = {"unitCode":"S1Code","unitName":"S1","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"Buy","quantity":270,"rate":25,"commission":MathUtil.round(1*270/300, 2)};
    unittest.assertEquals(true, n4t1.isIncremental());
    unittest.assertEquals(1, n4t1.getSign());
    unittest.assertEquals(BuyTransaction, n4t1.constructor);
    compareTransactions(n4t1, n4r1);  
  },

// Sell Transaction
  testSellTransaction : function () {
    var r1 = {"unitCode":"S1Code","unitName":"S1","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"Sell","quantity":300,"rate":25,"commission":1};
    var t1 = TransactionFactory.createTransaction(r1);
    unittest.assertEquals(false, t1.isIncremental());
    unittest.assertEquals(-1, t1.getSign());
    unittest.assertEquals(SellTransaction, t1.constructor);
    compareTransactions(t1, r1);
    var n1t1 = t1.splitTransaction(10);
    unittest.assertEquals(-290, t1.getQuantity());
    unittest.assertEquals(MathUtil.round(1*290/300, 2), t1.getCommission());
    var n1r1 = {"unitCode":"S1Code","unitName":"S1","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"Sell","quantity":10,"rate":25,"commission":MathUtil.round(1*10/300, 2)};
    unittest.assertEquals(false, n1t1.isIncremental());
    unittest.assertEquals(-1, n1t1.getSign());
    unittest.assertEquals(SellTransaction, n1t1.constructor);
    compareTransactions(n1t1, n1r1);
    var n2t1 = t1.splitTransaction(-20);
    unittest.assertEquals(-270, t1.getQuantity());
    unittest.assertEquals(MathUtil.round(1*270/300, 2), t1.getCommission());
    var n2r1 = {"unitCode":"S1Code","unitName":"S1","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"Sell","quantity":20,"rate":25,"commission":MathUtil.round(1*20/300, 2)};
    unittest.assertEquals(false, n2t1.isIncremental());
    unittest.assertEquals(-1, n2t1.getSign());
    unittest.assertEquals(SellTransaction, n2t1.constructor);
    compareTransactions(n2t1, n2r1);
    var n3t1 = t1.splitTransaction(0);
    unittest.assertEquals(-270, t1.getQuantity());
    unittest.assertEquals(MathUtil.round(1*270/300, 2), t1.getCommission());
    var n3r1 = {"unitCode":"S1Code","unitName":"S1","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"Sell","quantity":0,"rate":25,"commission":0};
    unittest.assertEquals(false, n3t1.isIncremental());
    unittest.assertEquals(-1, n3t1.getSign());
    unittest.assertEquals(SellTransaction, n3t1.constructor);
    compareTransactions(n3t1, n3r1);  
    var n4t1 = t1.splitTransaction(270);
    unittest.assertEquals(0, t1.getQuantity());
    unittest.assertEquals(0, t1.getCommission());
    var n4r1 = {"unitCode":"S1Code","unitName":"S1","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"Sell","quantity":270,"rate":25,"commission":MathUtil.round(1*270/300, 2)};
    unittest.assertEquals(false, n4t1.isIncremental());
    unittest.assertEquals(-1, n4t1.getSign());
    unittest.assertEquals(SellTransaction, n4t1.constructor);
    compareTransactions(n4t1, n4r1);  
  },

// Closed Transaction
  testClosedTransaction : function() {
    var d1 = new Date(2015, 5, 19);
    var d2 = new Date(2015, 5, 20);
    var r1 = {"unitCode":"A1","unitName":"A1","dateOfTransaction":d1,"transactionType":"Buy","quantity":100,"rate":5,"commission":0.5};
    var r2 = {"unitCode":"A1","unitName":"A1","dateOfTransaction":d2,"transactionType":"Sell","quantity":100,"rate":10,"commission":0.1};
    var r3 = {"unitCode":"A2","unitName":"A2","dateOfTransaction":d1,"transactionType":"Sell","quantity":100,"rate":15,"commission":0.2};
    var r4 = {"unitCode":"A2","unitName":"A2","dateOfTransaction":d2,"transactionType":"Buy","quantity":100,"rate":12,"commission":0.3};
    var r5 = {"unitCode":"A3","unitName":"A3","dateOfTransaction":d1,"transactionType":"Buy","quantity":100,"rate":15,"commission":0.5};
    var r6 = {"unitCode":"A3","unitName":"A3","dateOfTransaction":d2,"transactionType":"Sell","quantity":100,"rate":10,"commission":0.1};
    var r7 = {"unitCode":"A4","unitName":"A4","dateOfTransaction":d1,"transactionType":"Sell","quantity":100,"rate":5,"commission":0.5};
    var r8 = {"unitCode":"A4","unitName":"A4","dateOfTransaction":d2,"transactionType":"Buy","quantity":100,"rate":10,"commission":0.1};
    var t1 = TransactionFactory.createTransaction(r1);
    var t2 = TransactionFactory.createTransaction(r2);
    var t3 = TransactionFactory.createTransaction(r3);
    var t4 = TransactionFactory.createTransaction(r4);
    var t5 = TransactionFactory.createTransaction(r5);
    var t6 = TransactionFactory.createTransaction(r6);  
    var t7 = TransactionFactory.createTransaction(r7);  
    var t8 = TransactionFactory.createTransaction(r8);
    var ct1 = new ClosedTransaction(t1, t2);
    var ct2 = new ClosedTransaction(t3, t4);
    var ct3 = new ClosedTransaction(t5, t6);
    var ct4 = new ClosedTransaction(t7, t8);
    var ctE1 = {"unitCode":"A1","unitName":"A1","quantity":100,"dateOfAcquisition":"2015-06-19","rateOfAcquisition":5,"acquisitionCommission":0.5,"totalCostOfAcquisition":500.5,"dateOfDisposal":"2015-06-20","rateOfDisposal":10,"disposalCommission":0.1,"totalValueOfDisposal":999.9,"totalProfit":499.4,"holdingPeriod":1,"holdingTerm":"Short Term"};
    var ctE2 = {"unitCode":"A2","unitName":"A2","quantity":100,"dateOfAcquisition":"2015-06-20","rateOfAcquisition":12,"acquisitionCommission":0.3,"totalCostOfAcquisition":1200.3,"dateOfDisposal":"2015-06-19","rateOfDisposal":15,"disposalCommission":0.2,"totalValueOfDisposal":1499.8,"totalProfit":299.5,"holdingPeriod":1,"holdingTerm":"Short Term"};
    var ctE3 = {"unitCode":"A3","unitName":"A3","quantity":100,"dateOfAcquisition":"2015-06-19","rateOfAcquisition":15,"acquisitionCommission":0.5,"totalCostOfAcquisition":1500.5,"dateOfDisposal":"2015-06-20","rateOfDisposal":10,"disposalCommission":0.1,"totalValueOfDisposal":999.9,"totalProfit":-500.6,"holdingPeriod":1,"holdingTerm":"Short Term"};
    var ctE4 = {"unitCode":"A4","unitName":"A4","quantity":100,"dateOfAcquisition":"2015-06-20","rateOfAcquisition":10,"acquisitionCommission":0.1,"totalCostOfAcquisition":1000.1,"dateOfDisposal":"2015-06-19","rateOfDisposal":5,"disposalCommission":0.5,"totalValueOfDisposal":499.5,"totalProfit":-500.6,"holdingPeriod":1,"holdingTerm":"Short Term"};
    unittest.assertEquals(JSON.stringify(ctE1), JSON.stringify(ct1.getRow()));
    unittest.assertEquals(JSON.stringify(ctE2), JSON.stringify(ct2.getRow()));
    unittest.assertEquals(JSON.stringify(ctE3), JSON.stringify(ct3.getRow()));
    unittest.assertEquals(JSON.stringify(ctE4), JSON.stringify(ct4.getRow()));
  },

  testTransferOut : function(){
    var d1 = new Date(2015, 5, 19);
    var d2 = new Date(2015, 5, 20);
    var r1 = {"unitCode":"A1","unitName":"A1","dateOfTransaction":d1,"transactionType":"Transfer Out","quantity":120,"rate":5,"commission":0.7};
    var r2 = {"unitCode":"A1","unitName":"A1","dateOfTransaction":d2,"transactionType":"Transfer Out","quantity":150,"rate":10,"commission":0};
    var r3 = {"unitCode":"A2","unitName":"A2","dateOfTransaction":d1,"transactionType":"Transfer Out","quantity":100,"rate":15,"commission":0};
    var r4 = {"unitCode":"A6","unitName":"A6","dateOfTransaction":d2,"transactionType":"Transfer Out","quantity":10,"rate":12,"commission":0};
    try {
      var t1 = TransactionFactory.createTransaction(r1);
      unittest.assertFalse(true);
    } catch(e) {} //intentionally ignored
    var t2 = TransactionFactory.createTransaction(r2);
    var t3 = TransactionFactory.createTransaction(r3);
    var t4 = TransactionFactory.createTransaction(r4);
    var portfolio = new Portfolio;
    try{
      portfolio.addTransaction(t2);
      unittest.assertFalse(true);
    } catch(e){} //intentionally ignored
    unittest.assertEquals(false, t2.isIncremental());
    unittest.assertEquals(-100, t3.getQuantity());
    unittest.assertEquals("A2", t3.getUnitName());
    unittest.assertEquals(DateUtil.getDateString(d1), t3.getDateOfTransaction());
    unittest.assertEquals(15, t3.getRate());
    unittest.assertEquals(TransferOut, t2.constructor);
  },
 
  testFPO : function(){
    var r0 = {"unitCode":"S2Code","unitName":"S2","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"FPO","quantity":300,"rate":25,"commission":0.5};
    try {
      var t0 = TransactionFactory.createTransaction(r0);
      unittest.assertFalse(true)
    } catch(e) {}
    var r1 = {"unitCode":"S1Code","unitName":"S1","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"FPO","quantity":300,"rate":25,"commission":0};
    var t1 = TransactionFactory.createTransaction(r1);
    unittest.assertEquals(true, t1.isIncremental());
    unittest.assertEquals(1, t1.getSign());
    unittest.assertEquals(FPO, t1.constructor);
    compareTransactions(t1, r1);
    var n1t1 = t1.splitTransaction(10);
    unittest.assertEquals(290, t1.getQuantity());
    unittest.assertEquals(0, t1.getCommission());
    var n1r1 = {"unitCode":"S1Code","unitName":"S1","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"FPO","quantity":10,"rate":25,"commission":0};
    unittest.assertEquals(FPO, n1t1.constructor);
    compareTransactions(n1t1, n1r1);
    var n3t1 = t1.splitTransaction(0);
    var n3r1 = {"unitCode":"S1Code","unitName":"S1","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"FPO","quantity":0,"rate":25,"commission":0};
    unittest.assertEquals(FPO, n3t1.constructor);
    compareTransactions(n3t1, n3r1);  
    var n4t1 = t1.splitTransaction(290);
    unittest.assertEquals(0, t1.getQuantity());
    unittest.assertEquals(0, t1.getCommission());
    var n4r1 = {"unitCode":"S1Code","unitName":"S1","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"FPO","quantity":290,"rate":25,"commission":0};
    unittest.assertEquals(FPO, n4t1.constructor);
    compareTransactions(n4t1, n4r1);
    try{
      t1.splitTransaction(20);
      unittest.assertFalse(true);
    } catch(e) {}
  },

  testGiftIn : function(){
    var transactionFactory = new TransactionFactory;
    var unit = new Unit;
    var r0 = {"unitCode":"S2Code","unitName":"S2","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"Gift In","quantity":200,"rate":0,"commission":0.5};
    try{
      var t0 = TransactionFactory.createTransaction(r0);
      unittest.assertFalse(true);
    } catch(e) {}
    
    var r1 = {"unitCode":"S1Code","unitName":"S1","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"Gift In","quantity":100,"rate":25,"commission":0};
    try{
      var t1 = TransactionFactory.createTransaction(r1);
      unitetest.asserFalse(true);
    } catch(e){}
    
    var r2 = {"unitCode":"S1Code","unitName":"S1","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"Gift In","quantity":300,"rate":0,"commission":0};    
    var t2 = TransactionFactory.createTransaction(r2);
    unit.addTransaction(t2);
    unittest.assertEquals(true, t2.isIncremental());
    unittest.assertEquals(1, t2.getSign());
    unittest.assertEquals(GiftIn, t2.constructor);
    compareTransactions(t2, r2);
    
    var r3 = {"unitCode":"S1Code","unitName":"S1","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"Gift In","quantity":100,"rate":0,"commission":0};
    var t3 = TransactionFactory.createTransaction(r3);
    unit.addTransaction(t3);
    unittest.assertEquals(400, unit.getUnitCount());
    
    var r4 = {"unitCode":"S1Code","unitName":"S1","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"Buy","quantity":100,"rate":100,"commission":12};
    var t4 = TransactionFactory.createTransaction(r4)
    unit.addTransaction(t4);
    unittest.assertEquals(BuyTransaction, t4.constructor);
    unittest.assertEquals(500, unit.getUnitCount());
    
    var r5 = {"unitCode":"S1Code","unitName":"S1","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"Sell","quantity":350,"rate":66,"commission":1};
    var t5 = TransactionFactory.createTransaction(r5);
    unit.addTransaction(t5);
    unittest.assertEquals(150, unit.getUnitCount());
    unittest.assertEquals(2, unit.getOpenTransactions().length);
  },
  
  testGiftOut : function(){
    var transactionFactory = new TransactionFactory;
    var unit = new Unit;
    //trying to gift out when unit is 0
    var r0 = {"unitCode":"S0Code","unitName":"S2","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"Gift Out","quantity":200,"rate":0,"commission":0};
    try{
      var t0 = TransactionFactory.createTransaction(r0);
      unittest.assertFalse(true);
    } catch(e) {}
    
    //trying to gift out when rate not 0
    var r = {"unitCode":"S0Code","unitName":"S2","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"Gift Out","quantity":200,"rate":2,"commission":0};
    try{
        var t = TransactionFactory.createTransaction(r);
        unittest.assertFalse(true);
      } catch(e) {}
      
    var r1 = {"unitCode":"S1Code","unitName":"S2","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"Gift Out","quantity":100,"rate":0,"commission":0.3};
    try{
      var t1 = TransactionFactory.createTransaction(r1);
      unittest.assertFalse(true);
    } catch(e) {}
    
    var r2 = {"unitCode":"S2Code","unitName":"S2","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"Buy","quantity":100,"rate":0,"commission":0};
    var t2 = TransactionFactory.createTransaction(r2);
    unit.addTransaction(t2);
    unittest.assertEquals(true, t2.isIncremental());
    unittest.assertEquals(1, t2.getSign());
    unittest.assertEquals(BuyTransaction, t2.constructor);
    unittest.assertEquals(100, unit.getUnitCount());
    
    var r3 = {"unitCode":"S2Code","unitName":"S2","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"Gift Out","quantity":80,"rate":0,"commission":0};
    var t3 = TransactionFactory.createTransaction(r3);
    unit.addTransaction(t3);
    unittest.assertEquals(20, unit.getUnitCount());
    
    var r4 = {"unitCode":"S2Code","unitName":"S2","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"Gift In","quantity":100,"rate":0,"commission":0};
    var t4 = TransactionFactory.createTransaction(r4);
    unit.addTransaction(t4);
    unittest.assertEquals(120, unit.getUnitCount());
    Logger.log("Unitcount is " + unit.getUnitCount());
    
    var r5 = {"unitCode":"S2Code","unitName":"S2","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"Gift out","quantity":20,"rate":0,"commission":0};
    var t5 = TransactionFactory.createTransaction(r5);
    unit.addTransaction(t5);
    Logger.log("Unitcount is " + unit.getUnitCount());
    unittest.assertEquals(140, unit.getUnitCount());
    unittest.assertEquals(1, unit.getOpenTransactions().length);
    
    var r6 = {"unitCode":"S2Code","unitName":"S2","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"Gift Out","quantity":150,"rate":0,"commission":0};
    var t6 = TransactionFactory.createTransaction(r6);
    try {
      unit.addTransaction(t6);
      unittest.assertFalse(true);
    } catch(e) {}
  },

  testIPO : function(){
    var transactionFactory = new TransactionFactory;
    var unit = new Unit;
    //test when commission is 0
    var r0 = {"unitCode":"S2Code","unitName":"S2","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"IPO","quantity":300,"rate":25,"commission":0.5};
    try{
      var t0 = TransactionFactory.createTransaction(r0);
      unittest.assertFalse(true)
    } catch(e) {}
    
    //test when we already have closed transaction of same unit
    var r1 = {"unitCode":"S2Code","unitName":"S2","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"Buy","quantity":100,"rate":25,"commission":0.5};
    var t1 = TransactionFactory.createTransaction(r1);
    unit.addTransaction(t1);
    var r2 = {"unitCode":"S2Code","unitName":"S2","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"Sell","quantity":100,"rate":25,"commission":0.5};
    var t2 = TransactionFactory.createTransaction(r2);
    unit.addTransaction(t2);
    var r3 = {"unitCode":"S2Code","unitName":"S2","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"IPO","quantity":100,"rate":25,"commission":0};
    var t3 = TransactionFactory.createTransaction(r3);
    try {
      unit.addTransaction(t3);
      unittest.assertFalse(true);
    } catch(e) {}

    //test when we already have Open transaction of same unit
    var unit1 = new Unit;
    var r4 = {"unitCode":"S2Code","unitName":"S2","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"BUY","quantity":100,"rate":0,"commission":0};
    var t4 = TransactionFactory.createTransaction(r4);
    unit1.addTransaction(t4);
    var r5 = {"unitCode":"S2Code","unitName":"S2","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"IPO","quantity":250,"rate":25,"commission":0};
    var t5 = TransactionFactory.createTransaction(r5);
    try{
      unit1.addTransaction(t5);
      unittest.assertFalse(true);
    } catch(e) {}
    var unit2 = new Unit;
    var r6 = {"unitCode":"S2Code","unitName":"S2","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"IPO","quantity":150,"rate":25,"commission":0};
    var t6 = TransactionFactory.createTransaction(r6);
    unit2.addTransaction(t6);
    unittest.assertEquals(150, unit2.getUnitCount());
    
    var r7 = {"unitCode":"S2Code","unitName":"S2","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"Sell","quantity":50,"rate":25,"commission":0};
    var t7 = TransactionFactory.createTransaction(r7);
    unit2.addTransaction(t7);
    unittest.assertEquals(100, unit2.getUnitCount());
    unittest.assertEquals(1, unit2.openTransactions.length);
    unittest.assertEquals(1, unit2.closedTransactions.length);
    
    var r8 = {"unitCode":"S2Code","unitName":"S2","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"Sell","quantity":20,"rate":25,"commission":0};
    var t8 = TransactionFactory.createTransaction(r8);
    unit2.addTransaction(t8);
    unittest.assertEquals(80, unit2.getUnitCount());
    unittest.assertExactlyNotEqual("1", unit2.openTransactions.length);
    unittest.assertEquals(2, unit2.closedTransactions.length);
  },
  
  testBuyBack : function(){
    var transactionFactory = new TransactionFactory;
    var unit = new Unit;
    // test for buyback when commision not 0
    var r0 = {"unitCode":"S0Code","unitName":"S2","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"Buy Back","quantity":200,"rate":100,"commission":0.3};
    try{
      var t0 = TransactionFactory.createTransaction(r0);
      unittest.assertFalse(true);
    } catch(e) {}
    
    var r2 = {"unitCode":"S2Code","unitName":"S2","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"Buy","quantity":100,"rate":200,"commission":4};
    var t2 = TransactionFactory.createTransaction(r2);
    unit.addTransaction(t2);
    unittest.assertEquals(true, t2.isIncremental());
    unittest.assertEquals(1, t2.getSign());
    unittest.assertEquals(BuyTransaction, t2.constructor);
    unittest.assertEquals(100, unit.getUnitCount());
    
    var r3 = {"unitCode":"S2Code","unitName":"S2","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"Buy Back","quantity":80,"rate":250,"commission":0};
    var t3 = TransactionFactory.createTransaction(r3);
    unit.addTransaction(t3);
    unittest.assertEquals(20, unit.getUnitCount());
    
    var r4 = {"unitCode":"S2Code","unitName":"S2","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"Buy","quantity":100,"rate":150,"commission":1};
    var t4 = TransactionFactory.createTransaction(r4);
    unit.addTransaction(t4);
    unittest.assertEquals(120, unit.getUnitCount());
    Logger.log("Unitcount is " + unit.getUnitCount());
    
    var r5 = {"unitCode":"S2Code","unitName":"S2","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"Buy Back","quantity":20,"rate":180,"commission":0};
    var t5 = TransactionFactory.createTransaction(r5);
    unit.addTransaction(t5);
    Logger.log("Unitcount is " + unit.getUnitCount());
    unittest.assertEquals(100, unit.getUnitCount());
    unittest.assertEquals(1, unit.getOpenTransactions().length);
    
    //test when unit count is less than buyback quantity
    var r6 = {"unitCode":"S2Code","unitName":"S2","dateOfTransaction":new Date("2010-05-31T18:30:00.000Z"),"transactionType":"Buy Back","quantity":150,"rate":100,"commission":0};
    var t6 = TransactionFactory.createTransaction(r6);
    try {
      unit.addTransaction(t6);
      unittest.assertFalse(true);
    } catch(e) {}
  }
}

function compareTransactions(t1, row){
  unittest.assertEquals(row.unitName, t1.getUnitName());
  unittest.assertEquals(row.unitCode, t1.getUnitCode());
  unittest.assertEquals(row.transactionType, t1.getTransactionType());
  unittest.assertEquals(t1.getSign() * row.quantity, t1.getQuantity());
  unittest.assertEquals(row.getRate(), t1.getRate());
  unittest.assertEquals(row.commission, t1.getCommission());
  unittest.assertEquals(Math.abs(row.quantity), t1.getAbsoluteQuantity());
}
