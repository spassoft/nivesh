var OPEN_SHEET_NAME = 'Open Transactions';
var CURRENT_SHEET_NAME = 'Current Transactions';
var CLOSED_SHEET_NAME = 'Closed Transactions 2015-16';
var HOLDING_SHEET_NAME = 'Holding';

var STOCK_OPEN_SHEET_HEADER = ['unitCode', 'unitName', 'dateOfTransaction', 'transactionType', 'quantity', 'rate', 'commission'];
var STOCK_CURRENT_SHEET_HEADER = STOCK_OPEN_SHEET_HEADER;
var STOCK_CLOSED_SHEET_HEADER = ['unitCode', 'unitName', 'quantity', 'dateOfAcquisition', 'rateOfAcquisition', 'acquisitionCommission', 'totalCostOfAcquisition', 'dateOfDisposal', 'rateOfDisposal',
                           'disposalCommission', 'totalValueOfDisposal', 'totalProfit', 'holdingPeriod', 'holdingTerm'];
var STOCK_HOLDING_SHEET_HEADER = ['unitCode', 'unitName', 'quantity', 'averageRate'];

var MUTUAL_FUND_OPEN_SHEET_HEADER = ['mutualFundName','schemeName',	'schemeType', 'isin','dateOfTransaction', 'transactionType', 'quantity', 'nav', 'commission'];
var MUTUAL_FUND_CURRENT_SHEET_HEADER = MUTUAL_FUND_OPEN_SHEET_HEADER;
var MUTUAL_FUND_CLOSED_SHEET_HEADER = ['mutualFundName','schemeName', 'schemeType', 'isin','quantity', 'dateOfAcquisition', 'rateOfAcquisition', 'entryLoad', 'totalCostOfAcquisition',
                                       'dateOfDisposal', 'rateOfDisposal', 'exitLoad', 'totalValueOfDisposal', 'totalProfit', 'holdingPeriod', 'holdingTerm'];
var MUTUAL_FUND_HOLDING_SHEET_HEADER = ['mutualFundName','schemeName', 'schemeType', 'isin', 'quantity', 'averageNav'];
