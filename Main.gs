function updatePortfolio(spreadsheet, type) {
  ValidationUtil.isNotNull(spreadsheet, "No spreadsheet passed");
  var portfolio = PortfolioFactory.createPortfolio(type);
  var openSheetData = SpreadsheetUtil.getSheetData(spreadsheet.getSheetByName(OPEN_SHEET_NAME), portfolio.getOpenTransactionSheetHeader());
  var currentSheetData = SpreadsheetUtil.getSheetData(spreadsheet.getSheetByName(CURRENT_SHEET_NAME), portfolio.getCurrentTransactionSheetHeader());
  LogUtil.log(arguments.callee.name, "d", "Processing Open Transaction Sheet...");
  portfolio.process(openSheetData);
  LogUtil.log(arguments.callee.name, "d", "Processing Current Transaction Sheet...");
  portfolio.process(currentSheetData);
  SpreadsheetUtil.createBackup(spreadsheet, BACKUP_SHEET_SUFFIX);
  LogUtil.log(arguments.callee.name, "d", "Backup of Spreadsheet created");
  try {
    LogUtil.log(arguments.callee.name, "d", "Removing content and Writing header to Current Transaction Sheet-");
    portfolio.removeContentAndWriteHeader(spreadsheet.getSheetByName(CURRENT_SHEET_NAME), portfolio.getCurrentTransactionSheetHeader());
    LogUtil.log(arguments.callee.name, "d", "Writing Open Transactions to sheet-");
    portfolio.writeOpenTransactionsToSheet(spreadsheet.getSheetByName(OPEN_SHEET_NAME), portfolio.getOpenTransactionSheetHeader());
    LogUtil.log(arguments.callee.name, "d", "Writing Closed Transactions to sheet-");
    portfolio.writeClosedTransactionsToSheet(spreadsheet.getSheetByName(CLOSED_SHEET_NAME), portfolio.getClosedTransactionSheetHeader());
    LogUtil.log(arguments.callee.name, "d", "Writing Holding to sheet-");
    portfolio.writeHoldingToSheet(spreadsheet.getSheetByName(HOLDING_SHEET_NAME), portfolio.getHoldingSheetHeader());
    LogUtil.log(arguments.callee.name, "d", "Writing the sheets successful");
  }
  catch(e) {
    LogUtil.log(arguments.callee.name, "d", "Rollback - Error in writing to sheets : " + e);
    SpreadsheetUtil.rollback(spreadsheet, BACKUP_SHEET_SUFFIX); 
    throw "Rollback - Error in writing to sheets : " + e;
  }
  SpreadsheetUtil.dropBackupSheets(spreadsheet, BACKUP_SHEET_SUFFIX);
  LogUtil.log(arguments.callee.name, "d", "Deleted backup for Spreadsheet!");
}

function rollback(spreadsheet) {
  SpreadsheetUtil.rollback(spreadsheet, BACKUP_SHEET_SUFFIX);
}
