var PortfolioFactory = {
  createPortfolio: function (type) {
    switch(type) {
      case STOCK_TYPE:
        return new StockPortfolio(type);
      case MUTUAL_FUND_TYPE:
        return new MutualFundPortfolio(type);
      default:
        ValidationUtil.error(ERR_MSG_INVALID_PORTFOLIO_TYPE);    
    }
  }
}