var TransactionFactory = {
  createTransaction : function(row, unit) {
    switch(row.transactionType) {
      case BUY:
        return new BuyTransaction(row, unit);
      case SELL:
        return new SellTransaction(row, unit);
      case TRANSFER_IN:
        return new TransferIn(row, unit);
      case BONUS:
        return new BonusTransaction(row, unit);
      case FPO:
        return new FPO(row, unit);
      case TRANSFER_OUT:
        return new TransferOut(row, unit);
      case GIFT_IN:
        return new GiftIn(row, unit);
      case GIFT_OUT:
        return new GiftOut(row, unit);
      case IPO:
        return new IPO(row, unit);
      case BUY_BACK:
        return new BuyBack(row, unit);
      default:
        return new Transaction(row, unit);
    }
  }
}
