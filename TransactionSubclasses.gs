BuyTransaction = function(row, unit) {
  Transaction.call(this, row, unit);
  this.incremental = true;
}
BuyTransaction.prototype = Object.create(Transaction.prototype);
BuyTransaction.prototype.constructor = BuyTransaction;

SellTransaction = function(row, unit) {
  Transaction.call(this, row, unit);
  this.incremental = false;
  this.quantity = -MathUtil.round(row.quantity, CONFIG_OBJECT.quantityRoundOff);
}
SellTransaction.prototype = Object.create(Transaction.prototype);
SellTransaction.prototype.constructor = SellTransaction;

BonusTransaction = function(row, unit) {
  ValidationUtil.isEqual(row.rate, "0", "Rate:" + row.rate + " should be zero for Bonus Transaction");
  ValidationUtil.isEqual(row.commission, "0", "commission:" + row.commission + " should be zero for Bonus Transaction");
  Transaction.call(this, row, unit);
  this.incremental = true;
}
BonusTransaction.prototype = Object.create(Transaction.prototype);
BonusTransaction.prototype.constructor = BonusTransaction;
	
TransferIn = function(row, unit) {
  ValidationUtil.isEqual(row.commission, "0", "commission:" + row.commission + " should be zero for TransferIn Transaction");
  Transaction.call(this, row, unit);
  this.incremental = true;
} 
TransferIn.prototype = Object.create(Transaction.prototype);
TransferIn.prototype.constructor = TransferIn;

TransferOut = function(row, unit) {
  ValidationUtil.isEqual(row.commission, "0", "commission:" + row.commission + " should be zero for TransferOut Transaction");
  Transaction.call(this, row, unit);
  this.incremental = false;
  this.quantity = -MathUtil.round(row.quantity, CONFIG_OBJECT.quantityRoundOff);
} 
TransferOut.prototype = Object.create(Transaction.prototype);
TransferOut.prototype.constructor = TransferOut;

FPO = function(row, unit) {
  ValidationUtil.isEqual(row.commission, "0", "commission:" + row.commission + " should be zero for FPO Transaction");
  Transaction.call(this, row, unit);
  this.incremental = true;
}
FPO.prototype = Object.create(Transaction.prototype);
FPO.prototype.constructor = FPO;

GiftIn = function(row, unit) {
  ValidationUtil.isEqual(row.getRate(), "0", "Rate:" + row.getRate() + " should be zero for GiftIn Transaction");
  ValidationUtil.isEqual(row.commission, "0", "commission:" + row.commission + " should be zero for GiftIn Transaction");
  Transaction.call(this, row, unit);
  this.incremental = true;
}
GiftIn.prototype = Object.create(Transaction.prototype);
GiftIn.prototype.constructor = GiftIn;

GiftOut = function(row, unit) {
  ValidationUtil.isEqual(row.commission, "0", "commission:" + row.commission + " should be zero for GiftOut Transaction");
  ValidationUtil.isEqual(row.getRate(), "0", "Rate:" + row.getRate() + " should be zero for GiftOut Transaction");
  Transaction.call(this, row, unit);
  this.incremental = false;
  this.quantity = -MathUtil.round(row.quantity, CONFIG_OBJECT.quantityRoundOff);
} 
GiftOut.prototype = Object.create(Transaction.prototype);
GiftOut.prototype.constructor = GiftOut;

IPO = function(row, unit) {
  ValidationUtil.isEqual(row.commission, "0", "commission:" + row.commission + " should be zero for IPO Transaction");
  Transaction.call(this, row, unit);
  this.incremental = true;
}
IPO.prototype = Object.create(Transaction.prototype);
IPO.prototype.constructor = IPO;

BuyBack = function(row, unit) {
  ValidationUtil.isEqual(row.commission, "0", "commission:" + row.commission + " should be zero for BuyBack Transaction");
  Transaction.call(this, row, unit);
  this.incremental = false;
  this.quantity = -MathUtil.round(row.quantity, CONFIG_OBJECT.quantityRoundOff);
}
BuyBack.prototype = Object.create(Transaction.prototype);
BuyBack.prototype.constructor = BuyBack;
