ClosedTransaction = function(newTransaction, oldTransaction) {
  this.incrementalTransaction = newTransaction.isIncremental() == true ? newTransaction : oldTransaction;
  this.decrementalTransaction = newTransaction.isIncremental() == false ? newTransaction : oldTransaction;
}

ClosedTransaction.prototype.writeToSheet = function(sheet, headers) {
  var row = this.createRow();
  LogUtil.log(arguments.callee.name, "d", "Writing Closed Transaction : " + JSON.stringify(row));
  SpreadsheetUtil.writeARow(sheet, headers, row);
}

ClosedTransaction.prototype.createRow = function() {
  var row = {};
  this.incrementalTransaction.unit.fillIdentifierProperties(row);
  row.quantity = this.incrementalTransaction.getAbsoluteQuantity();
  row.dateOfAcquisition = this.incrementalTransaction.getDateOfTransaction();
  row.rateOfAcquisition = this.incrementalTransaction.getRate();
  row.acquisitionCommission = this.incrementalTransaction.getCommission();
  row.entryLoad = this.incrementalTransaction.getCommission();
  row.totalCostOfAcquisition = MathUtil.round(row.rateOfAcquisition * row.quantity + row.acquisitionCommission, 2);
  row.dateOfDisposal = this.decrementalTransaction.getDateOfTransaction();
  row.rateOfDisposal = this.decrementalTransaction.getRate();
  row.disposalCommission = this.decrementalTransaction.getCommission();
  row.exitLoad = this.decrementalTransaction.getCommission();
  row.totalValueOfDisposal = MathUtil.round(row.rateOfDisposal * row.quantity - row.disposalCommission, 2);
  row.totalProfit = row.totalValueOfDisposal - row.totalCostOfAcquisition;
  row.holdingPeriod = Math.abs(DateUtil.getDaysBetweenDates(DateUtil.convertStringToDateObject(this.incrementalTransaction.getDateOfTransaction()), 
                                                                 DateUtil.convertStringToDateObject(this.decrementalTransaction.getDateOfTransaction())));
  row.holdingTerm = row.holdingPeriod >= CONFIG_OBJECT.stcgLimit ? LONG_TERM : SHORT_TERM;
  return row;
}
