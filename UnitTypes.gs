StockUnit  = function(row) {
  Unit.call(this);
  this.unitCode = row.unitCode;
  this.unitName = row.unitName;
  this.averageRate = null;
};
StockUnit.prototype = Object.create(Unit.prototype);
StockUnit.prototype.constructor = StockUnit;

StockUnit.prototype.equals = function(row) {
  return this.unitCode == row.unitCode;
}

StockUnit.prototype.fillIdentifierProperties = function(obj) {
  var identifiers = ["unitCode", "unitName"];
   for(var i in identifiers) {
    obj[identifiers[i]] = this[identifiers[i]];
  }
}

StockUnit.prototype.getHoldingRow = function() {
  var holdingRow = {};
  this.fillIdentifierProperties(holdingRow);
  holdingRow.averageRate = this.computeAverageRate();
  holdingRow.quantity = this.unitCount;
  return holdingRow;
};

MutualFundUnit  = function(row) {
  Unit.call(this);
  this.mutualFundName = row.mutualFundName;
  this.schemeName = row.schemeName;
  this.schemeType = row.schemeType;
  this.isin = row.isin;
  this.averageNav = null;
};
MutualFundUnit.prototype = Object.create(Unit.prototype);
MutualFundUnit.prototype.constructor = MutualFundUnit;

MutualFundUnit.prototype.equals = function(row) {
  return this.isin == row.isin;
}

MutualFundUnit.prototype.fillIdentifierProperties = function(obj) {
  var identifiers = ["mutualFundName", "schemeName", "schemeType", "isin"];
    for(var i in identifiers) {
    obj[identifiers[i]] = this[identifiers[i]];
  }
}

MutualFundUnit.prototype.getHoldingRow = function() {
  var holdingRow = {};
  this.fillIdentifierProperties(holdingRow);
  holdingRow.averageNav = this.computeAverageRate();
  holdingRow.quantity = this.unitCount;
  return holdingRow;
};

