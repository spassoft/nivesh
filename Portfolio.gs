Portfolio  = function(type) {
  ValidationUtil.isNotNull(type, ERR_MSG_PORTFOLIO_NULL_TYPE);
  this.units = [];
  this.type = type;
};

Portfolio.prototype.addUnit = function(row) {
  var unit = UnitFactory.createUnit(row, this.type);
  this.units.push(unit);
  return unit;
}

Portfolio.prototype.getUnits = function() {
  return this.units;
}

Portfolio.prototype.getUnit = function(row) {
  for(var i in this.units) {
    var unit = this.units[i];
    if(unit.equals(row)) {
      LogUtil.log(arguments.callee.name, "d", "Unit already exists with unit count: " + unit.getUnitCount());
      return unit;
    }
  }
  LogUtil.log(arguments.callee.name, "d", "No existing Unit, adding");
  return this.addUnit(row);
}

Portfolio.prototype.process = function(sheetData) {
  for(var i in sheetData) {
    var row = sheetData[i];    
    LogUtil.log(arguments.callee.name, "d", (+i+1) + "th row : " + JSON.stringify(row));
    var unit = this.getUnit(row);
    var transaction = TransactionFactory.createTransaction(row, unit);
    unit.addTransaction(transaction);
  }
}

Portfolio.prototype.getType = function() {
  return this.type;
}

Portfolio.prototype.writeOpenTransactionsToSheet = function(sheet, headers) {
  sheet.clearContents();
  SpreadsheetUtil.writeHeaders(sheet, headers);
  for(var i in this.units) {
    this.units[i].writeOpenTransactionsToSheet(sheet, headers);
  }
}

Portfolio.prototype.writeClosedTransactionsToSheet = function(sheet, headers) {
  if(sheet.getLastColumn() == 0) {
    SpreadsheetUtil.writeHeaders(sheet, headers)
  }
  for(var i in this.units) {
    this.units[i].writeClosedTransactionsToSheet(sheet, headers);
  }
}

Portfolio.prototype.writeHoldingToSheet = function(sheet, headers) {
  sheet.clearContents();
  SpreadsheetUtil.writeHeaders(sheet, headers);
  for(var i in this.units) {
    if(this.units[i].getUnitCount() != 0) {
      this.units[i].writeHoldingRow(sheet, headers);
    }
  }
}

Portfolio.prototype.removeContentAndWriteHeader = function(sheet, headers) {
  sheet.clearContents();
  SpreadsheetUtil.writeHeaders(sheet, headers);
}

Portfolio.prototype.getOpenTransactionSheetHeader = function() {
  return this.openTransactionSheetHeader;
}

Portfolio.prototype.getCurrentTransactionSheetHeader = function() {
  return this.currentTransactionSheetHeader;
}

Portfolio.prototype.getClosedTransactionSheetHeader = function() {
  return this.closedTransactionSheetHeader;
}

Portfolio.prototype.getHoldingSheetHeader = function() {
  return this.holdingSheetHeader;
}
