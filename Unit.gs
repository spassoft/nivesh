Unit  = function() {
  this.openTransactions = [];
  this.closedTransactions = [];
  this.unitCount = 0;
};

Unit.prototype.addTransaction = function(transaction) {
  LogUtil.log(arguments.callee.name, "d", "Adding current transaction to the unit");
  this.validateUnit(transaction);
  this.increaseUnitCount(transaction.getQuantity());
  LogUtil.log(arguments.callee.name, "d", "New unit count: " + this.getUnitCount());
  if(this.openTransactions[0] && this.openTransactions[0].isIncremental() == !transaction.isIncremental()) {
    LogUtil.log(arguments.callee.name, "d", "Unit has open transactions with different sign.");
    var currentTransactionSquaredUp = false;
    for(var i = 0; i < this.openTransactions.length;) {
      var aq1 = transaction.getAbsoluteQuantity();
      LogUtil.log(arguments.callee.name, "d", "Current transaction quantity: " + transaction.getQuantity());
      var openTransaction = this.openTransactions[i];
      var aq2 = openTransaction.getAbsoluteQuantity();
      if(aq1 > aq2) {
        LogUtil.log(arguments.callee.name, "d", "Splitting Current transaction as quantity is greater");
        var newTransaction = transaction.splitTransaction(aq2);
        var closedTransaction = new ClosedTransaction(newTransaction, openTransaction);
        this.closedTransactions.push(closedTransaction);
        this.openTransactions.shift();
      }
      else if(aq1 == aq2) {
        LogUtil.log(arguments.callee.name, "d", "Quantity is same, so squaring off");
        var closedTransaction = new ClosedTransaction(transaction, openTransaction);
        this.closedTransactions.push(closedTransaction);
        this.openTransactions.shift();
        currentTransactionSquaredUp = true;
        break;
      }
      else if(aq2 > aq1) {
        LogUtil.log(arguments.callee.name, "d", "Splitting Open transaction as quantity is greater");
        var newOpenTransaction = openTransaction.splitTransaction(aq1);
        var closedTransaction = new ClosedTransaction(transaction, newOpenTransaction);
        this.closedTransactions.push(closedTransaction);
        currentTransactionSquaredUp = true;
        break;
      }
    }
    if(!currentTransactionSquaredUp) {
      LogUtil.log(arguments.callee.name, "d", "Adding remaining to Open Transactions");
      this.openTransactions.push(transaction);
    }
  }
  else {
    LogUtil.log(arguments.callee.name, "d", "No Open Transaction or Same sign Transaction, so adding");
    this.openTransactions.push(transaction);
  }
  LogUtil.log(arguments.callee.name, "d", "Transaction added successfully.");
};

Unit.prototype.getBlackListedPropertyArray = function() {
  var blackList = ["openTransactions", "closedTransactions"];
  return blackList;
};

Unit.prototype.stringify = function() {
  return JSON.stringify(this, ObjectUtil.getWhiteListedPropertyArray(this, this.getBlackListedPropertyArray()));
};

Unit.prototype.writeOpenTransactionsToSheet = function(sheet, headers) {
  for(var i in this.openTransactions) {
    var openTransaction = this.openTransactions[i];
    openTransaction.writeToSheet(sheet, headers);
  }
};

Unit.prototype.writeClosedTransactionsToSheet = function(sheet, headers) {
  for(var i in this.closedTransactions) {
    var closedTransaction = this.closedTransactions[i];
    closedTransaction.writeToSheet(sheet, headers);
  }
};

Unit.prototype.writeHoldingRow = function(sheet, headers) {
  var holdingRow = this.getHoldingRow();
  LogUtil.log(arguments.callee.name, "d", "Writing Holding row : " + JSON.stringify(holdingRow));
  SpreadsheetUtil.writeARow(sheet, headers, holdingRow);
};

Unit.prototype.computeAverageRate = function() {
  var averageRate = 0;
  for(var i in this.openTransactions) {
    var openTransaction = this.openTransactions[i];
    averageRate += ((openTransaction.getAbsoluteQuantity() * openTransaction.getRate()) + (openTransaction.getCommission() * openTransaction.getSign())) * openTransaction.getSign();
  }
  averageRate = MathUtil.round(averageRate/(this.getUnitCount()), 2);
  return Math.abs(averageRate);
};

Unit.prototype.getUnitCount = function() {
  return this.unitCount;
};

Unit.prototype.increaseUnitCount = function(n) {
  Logger.log(typeof n);
  this.unitCount += n;
};

Unit.prototype.decreaseUnitCount = function(n) {
  this.unitCount -= n;
};

Unit.prototype.addOpenTransaction = function(openTransaction) {
  this.openTransactions.push(openTransaction);
  this.increaseUnitCount(openTransaction.getQuantity());
};

Unit.prototype.getOpenTransactions = function() {
  return this.openTransactions;
};

Unit.prototype.addClosedTransaction = function(closedTransaction) {
  this.closedTransactions.push(closedTransaction);
};

Unit.prototype.getClosedTransactions = function() {
  return this.closedTransactions;
};

Unit.prototype.validateUnit = function(transaction) {
  var type = transaction.getTransactionType();
  if((type == TRANSFER_OUT || type == GIFT_OUT || type == BUY_BACK) && (transaction.getQuantity() + this.unitCount) < 0){
    throw "Error: Insufficient Unit Count for a " + type + " Transaction";
  }
  else if( type == IPO && (this.openTransactions.length > 0 || this.closedTransactions.length > 0)){
    throw "Error: Invalid IPO Transaction";       
  }
};