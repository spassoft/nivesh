var UnitFactory = {
  createUnit: function (row, type) {
    switch(type) {
      case STOCK_TYPE:
        return new StockUnit(row);
      case MUTUAL_FUND_TYPE:
        return new MutualFundUnit(row);
      default:
        ValidationUtil.error(ERR_MSG_INVALID_PORTFOLIO_TYPE);
    }
  }
}