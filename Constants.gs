var BACKUP_SHEET_SUFFIX = ' Backup';

var LONG_TERM = 'Long Term';

var SHORT_TERM = 'Short Term';

var STOCK_TYPE = "stock";

var MUTUAL_FUND_TYPE = "mutualFund";

var BUY = "Buy";

var SELL = "Sell";

var TRANSFER_IN = "Transfer In";

var TRANSFER_OUT = "Transfer Out";

var GIFT_IN = "Gift In";

var GIFT_OUT = "Gift Out";

var BONUS = "Bonus";

var FPO = "FPO";

var IPO = "IPO";

var BUY_BACK = "Buy Back";

// stcgLimit is Short Term Capital Gain. It is used in ClosedTransaction.gs to determine Long Term and Short Term holding periods 
var CONFIG_OBJECT = {"stcgLimit" : 365, "quantityRoundOff" : 5};
