StockPortfolio  = function(type) {
  Portfolio.call(this, type);
  this.openTransactionSheetHeader = STOCK_OPEN_SHEET_HEADER;
  this.currentTransactionSheetHeader = STOCK_CURRENT_SHEET_HEADER;
  this.closedTransactionSheetHeader = STOCK_CLOSED_SHEET_HEADER;
  this.holdingSheetHeader = STOCK_HOLDING_SHEET_HEADER;
};
StockPortfolio.prototype = Object.create(Portfolio.prototype);
StockPortfolio.prototype.constructor = StockPortfolio;

MutualFundPortfolio  = function(type) {
  Portfolio.call(this, type);
  this.openTransactionSheetHeader = MUTUAL_FUND_OPEN_SHEET_HEADER;
  this.currentTransactionSheetHeader = MUTUAL_FUND_CURRENT_SHEET_HEADER;
  this.closedTransactionSheetHeader = MUTUAL_FUND_CLOSED_SHEET_HEADER;
  this.holdingSheetHeader = MUTUAL_FUND_HOLDING_SHEET_HEADER;
};
MutualFundPortfolio.prototype = Object.create(Portfolio.prototype);
MutualFundPortfolio.prototype.constructor = MutualFundPortfolio;